<?php
class MythAdmin{
    /// Myth Admin Configuration Options
    public $config = array(
        'themename' => 'MythAdmin',
        'themefoldername' => 'myth-admin-framework',
        'themever' => '1.0.1',
        'shortname' => 'myth',
        'cssprefix' => 'myth_',
        'menus' => array('Main Menu', 'Top Bar Menu', 'Footer Menu'),
        'widgets' => array('Homepage Sidebar', 'Blog Sidebar'),
        'admin_page_title' => 'Myth Admin',
        'admin_menu_icon' => '/myth-admin-framework/mythadmin/images/mythadminicon.png',
    );
    
    // Myth Admin Theme Options (Contains Categories-> Subcategories -> Options)
    public $themeopts = array(
        'Sample Category' => array(
            'Sample Sub Category' => array(
                                array(
                                    'name'=>'Sample Text Option',
                                    'id'=>'sample_text_option',
                                    'type'=>'text',
                                    'val'=>''
                                ),
                                array(
                                    'id'=>'sample_color_option',
                                    'name'=>'Sample Color Option',
                                    'type'=>'color',
                                    'val'=>'ffffff'
                                ),
                                array(
                                    'name'=>'Sample Select Option',
                                    'id'=>'sample_select_option',
                                    'type'=>'select',
                                    'options'=>array('opt1','opt2','opt3','opt4'),
                                    'val'=>'opt1'
                                ),
                                array(
                                    'name'=>'Sample On/Off Button',
                                    'type'=>'onoff',
                                    'id'=>'sample_onoff_button',
                                    'val'=>'checked'
                                ),
                                array(
                                    'name'=>'Sample Yes/No Button',
                                    'type'=>'yesno',
                                    'id'=>'sample_yesno_button',
                                    'val'=>'checked'
                                ),
                                array(
                                    'name'=>'Sample Slider Option',
                                    'id'=>'sample_slider_option',
                                    'type'=>'slider',
                                    'min'=>0,
                                    'max'=>50,
                                    'step'=>5,
                                    'val_label'=>'px',
                                    'val'=>10
                                ),
                                array(
                                    'name'=>'Sample Text Area',
                                    'id'=>'sample_text_area',
                                    'type'=>'textarea',
                                    'val'=>''
                                ),
                                array(
                                    'name'=>'Sample Image Upload',
                                    'type'=>'file',
                                    'id'=>'sample_file_upload',
                                    'val'=>'',
                                    'upload_title'=>'Upload Image'
                                ),
            ),
        ),
    );
    
    // Myth Admin Framework Dict
    public $myth = array(
        'name' => 'Myth Admin Framework',
        'ver' => '1.0.1'
    );
    
    /*//////////////////////////////////////////
    /* MythAdmin Framework Class Functions/////
    /*////////////////////////////////////////
    
    
    public function register_theme(){
        // Register the theme menus & sidebars
        $this->myth_menus($this->config['menus']);
        $this->myth_widgets($this->config['widgets']);
        
        // Add Admin Page
        $this->add_myth_scripts();
        $this->register_admin_page();
        
    }
    
    // Function to Add Myth Frameworks CSS and JS files
    private function add_myth_scripts(){
        wp_enqueue_style('thickbox');
        wp_enqueue_style('myth_admin_stylesheet', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/css/mythadmin.css');
        wp_enqueue_style('myth_checkboxes_stylesheet', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/css/checkboxes.css');
        wp_enqueue_style('myth_jquery_ui_stylesheet', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/css/jquery-ui.css');
        wp_enqueue_style('myth_admin_colorpicker_stylesheet', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/colorpicker/css/colorpicker.css');
        wp_enqueue_script('myth_admin_easing_js', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/js/jquery.easing.js', array('jquery'));
        wp_enqueue_script('myth_admin_jquery_ui', 'http://code.jquery.com/ui/1.8.23/jquery-ui.min.js', array('jquery'));
        wp_enqueue_script('myth_admin_colorpicker_js', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/colorpicker/js/colorpicker.js', array('jquery'));
        wp_enqueue_script('myth_checkbox_js', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/js/iphone-style-checkboxes.js', array('jquery'));
        wp_enqueue_script('myth_slimScroll_js', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/js/slimScroll.min.js', array('jquery'));
        wp_enqueue_script('myth_admin_js', get_theme_root_uri().'/'.$this->config['themefoldername'].'/mythadmin/js/mythadmin.js', array('jquery'));
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        add_action('wp_ajax_updateOptionsAjax', array($this, 'updateOptionsAjax'));// For Ajax Submission of Theme options Form.
        //add_action('wp_ajax_nopriv_updateOptionsAjax', array($this, 'updateOptionsAjax'));// For Ajax Submission of Theme options Form.
    }
    
    // Function to Register Navigation Menus in Wordpress Admin
    private function myth_menus($menus){
        $mythmenus = array();
        foreach($menus as $menu){
            $uniquename = $this->uniquify($menu);
            $mythmenus[$uniquename] = __($menu, $this->config['shortname']);
        }
        register_nav_menus($mythmenus);
    }
    
    private function myth_widgets($widgets){
        foreach($widgets as $widget){
            register_sidebar(array(
                    'name' => __($widget, $this->config['shortname']),
                    'id' => $this->uniquify($widget),
                    'before_widget' => '<div id="%1$s" class="widget %2$s">',
                    'after_widget' => '</div>',
                    'before_title' => '<h3 class="widget-title">',
                    'after_title' => '</h3>'));
        }
    }
    
    private function register_admin_page(){
        add_action('admin_menu', array($this,'myth_add_admin_page'));
    }
    
    private function _get_file($partial_path){
        $filepath = get_theme_root_uri().$partial_path;
        return $filepath;
    }
    
    private function uniquify($string){
        $rsp = str_replace(' ', '-', $string);
        $low = strtolower($rsp);
        return $low;
    }
    
    private function toIdClass($string){
        $prefix = $this->config['cssprefix'];
        $rsp = str_replace(' ','_', $string);
        $low = strtolower($rsp);
        $res = $prefix.$low;
        return $res;
    }
    
    private function updateOptions($opts){
        foreach($opts as $optcat){
            foreach($optcat as $optscat){
                foreach($optscat as $option){
                    update_option($option['id'], $_REQUEST[$option['id']]);
                }
            }
        }
    }
    
    private function updateOptionIsset($opts){
        foreach($opts as $optcat){
            foreach($optcat as $optscat){
                foreach($optscat as $option){
                    if (isset($_REQUEST[$option['id']])){
                        update_option($option['id'], $_REQUEST[$option['id']]);
                    } else {
                        // Pass #Debug
                    }
                }
            }
        }
    }
    
    private function deleteOptions($opts){
        foreach($opts as $optcat){
            foreach($optcat as $optscat){
                foreach($optscat as $option){
                    delete_option($option['id']);
                }
            }
        }
    }
    
    private function updateModifyOptions($opts){
        // Update options on Modification
        if($_POST['theme_form_submit']){
            $this->updateOptions($opts);
        }
        // Create options if not Set
        $this->updateOptionIsset($opts);
        
        // Delete Options on Reset
        if($_POST['reset']){
            $this->deleteOptions($opts);
        }
    }
    
    /*/////////// Wordpress Based Functions///*/
    
    function myth_add_admin_page(){
            $conf = $this->config;
            $opts = $this->themeopts;
            $this->updateModifyOptions($opts);
            add_menu_page($conf['admin_page_title'],$conf['themename'], 'manage_options', $this->uniquify($conf['admin_page_title']), array($this, 'myth_admin_page'), $this->_get_file($conf['admin_menu_icon']));
    }
    
    function updateOptionsAjax(){
        foreach($_POST as $k => $v){
            update_option($k, $v);
        }
        die('1');
    }
    
    function myth_admin_page(){
        $opts = $this->themeopts;
        ?>
        <div class="myth-wrap">
            <div id="myth-topbar">
                <div id="myth-theme-logo">
                    <?php echo $this->config['themename']; ?> (v <?php echo $this->config['themever'] ?>)
                </div>
            </div>
            <div id="myth-sidebar">
                <div  class="fixedEl" id="myth-sidebar-inner">
                <?php
                    foreach($opts as $cat => $scat){
                    ?>
                    <div class="mythcat-wrap">
                        <div id="<?php echo $this->toIdClass($cat); ?>" class="mythcat"><?php echo $cat; ?></div>
                            <ul class="myth-subcat" for="<?php echo $this->toIdClass($cat); ?>">
                            <?php
                                foreach($scat as $sub => $sval){
                                ?>
                                <li id="<?php echo $this->toIdClass($sub); ?>"><?php echo $sub; ?></li>
                                <?php } ?>
                            </ul>
                    </div>
                    <?php } ?>
                    <div class="myth-credits">
                        <p>Powered By: <?php echo $this->myth['name']; ?> (v <?php echo $this->myth['ver']; ?>)</p>
                    </div>
                </div>
            </div>
            <div id="myth-admin-main-wrap">
                <div class="notifications">
                    <!-- Theme Administration Page Starts -->
                    <?php
                    // Add Notifications
                    if($_GET['notice']){
                        $mssg = '<div class="myth-notification myth-notice">%s</div>';
                        echo sprintf($mssg, $_GET['notice']);
                    } else if($_GET['error']) {
                        $mssg = '<div class="myth-notification myth-error">%s</div>';
                        echo sprintf($mssg, $_GET['error']);
                    } else if($_GET['success']) {
                        $mssg = '<div class="myth-notification myth-success">%s</div>';
                        echo sprintf($mssg, $_GET['success']);
                    }
                    ?>
                </div>
                <?php
                    foreach($opts as $cat => $scat){
                        foreach($scat as $k => $v){
                            ?>
                            <div class="mythSCFormContainer" id="<?php echo $this->toIdClass($k); ?>">
                            <h2 class="subCat-title"><?php echo $k; ?></h2>
                            <form method="post" id="<?php echo $this->uniquify($k); ?>_form" class="myth-form">
                            <?php
                            foreach($v as $option){
                                switch($option['type']){
                                    
                                    // Case Colorpicker Field
                                    case "color":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <input class="myth_colorpicker" type="text" id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>" value="<?php if(get_option($option['id']) != ''){ echo get_option($option['id']); } else {echo $option['val'];} ?>" />
                                    </div>
                                    <?php
                                    break;
                                    // Case Drop Downs / Selectbox
                                    case "select":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <select id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>">
                                            <?php
                                            if (get_option($option['id']) != ''){
                                                foreach($option['options'] as $v){
                                                    if ($v == get_option($option['id'])){
                                                    ?>
                                                    <option selected="selected" value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                    <?php
                                                    }
                                                }
                                            } else {
                                                foreach($option['options'] as $v){
                                                    if ($v == $value['val']){
                                                    ?>
                                                    <option selected="selected" value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                    <?php
                                                    } else {
                                                    ?>
                                                    <option value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <?php
                                    break;
                                    
                                    // Case Text Fields
                                    case "text":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text" id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>" value="<?php if(get_option($option['id']) != ''){ echo get_option($option['id']); } else {echo $option['val'];} ?>" />
                                    </div>
                                    <?php
                                    break;
                                
                                    // Case Textarea
                                    case "textarea":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>"><?php if(get_option($option['id']) != ''){ echo get_option($option['id']); } else {echo $option['val'];} ?></textarea>
                                    </div>
                                    <?php
                                    break;
                                
                                    // Case File/Media upload
                                    case "file":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text" id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>" value="<?php if(get_option($option['id']) != ''){ echo get_option($option['id']); } else {echo $option['val'];} ?>" />
                                        <input type="button" class="upload_button" value="<?php echo $option['upload_title']; ?>" for="<?php echo $option['id']; ?>" />
                                    </div>
                                    <?php
                                    break;
                                
                                    // Case On/Off
                                    case "onoff":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="checkbox" class="onoff" id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>" <?php if(get_option($option['id']) == 'on'){ ?> checked="<?php echo get_option($option['id']); ?>"<?php } ?> />
                                    </div>
                                    <?php
                                    break;
                                
                                    // Case Yes/No
                                    case "yesno":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="checkbox" class="yesno" id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>" <?php if(get_option($option['id']) == 'on'){ ?> checked="<?php echo get_option($option['id']); ?>"<?php } ?> />
                                    </div>
                                    <?php
                                    break;
                                
                                    // Case Slider
                                    case "slider":
                                    ?>
                                    <div class="<?php echo $this->config['cssprefix']; ?>option_container">
                                        <div class="mythSliderOption">
                                            <label class="myth_label" for="<?php echo $option['id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="theMythSlider" id="<?php echo $option['id']; ?>_slider">
                                                <div class="slVal">
                                                    <input type="text" class="sliderVal" id="<?php echo $option['id']; ?>" name="<?php echo $option['id']; ?>" value="<?php if(get_option($option['id']) != ''){ echo get_option($option['id']); } else {echo $option['val'];} ?>" />
                                                    <span class="mythSliderSuffix"><?php echo $option['val_label']; ?></span>
                                                </div>
                                                <input type="hidden" class="sliderMin" value="<?php echo $option['min']; ?>" />
                                                <input type="hidden" class="sliderMax" value="<?php echo $option['max']; ?>" />
                                                <input type="hidden" class="sliderStep" value="<?php echo $option['step']; ?>" />
                                            </div>
                                        </div>
                                    </div>
                            <?php }
                            } ?>
                            <input type="submit" value="Update Options" id="theme_form_submit" for="<?php echo $this->uniquify($k); ?>" />
                            </form></div>
                            <?php
                        }
                    }
                ?>
            </div>
        </div>
    <?php
    }
}
?>