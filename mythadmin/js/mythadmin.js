var $ = jQuery;

$(document).ready(function(){
    add_dismiss_notification();
    initNoticeDismissal();
    $('div.mythcat').click(function(){
        var cat = $(this).attr('id');
        showSubCat(cat);
    });
    
    // Initialize Colorpicker
    $('.myth_colorpicker').ColorPicker({
        onSubmit: function(hsb, hex, rgb, el) {
		$(el).val(hex);
		$(el).ColorPickerHide();
	},
        onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
    })
   .bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
   });
   
   
   // Show Sub Category Pages
   $('.myth-subcat li').click(function(){
	showSubCatPage($(this));
    });
   
   // Create Sliders
   createSliders();
   
   // Initiate Scrollers for Fixed Positioned Elements
   $('.fixedEl').slimScroll({
	height:'610px'
    });
   
   // Initialize Built-in Media Uploader on Custom Upload Fields
   $('.upload_button').click(function(){
      tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
      fileuploadfield = '#'+$(this).attr('for');
   });
   window.send_to_editor = function(html){
         url = $('img',html).attr('src');
         $(fileuploadfield).val(url);
         tb_remove();
   }
   
   // Theme Options Page form Ajax Update
   $('.myth-form').submit(function(){
	var fdata = $(this).serialize();
	// Unchecked checkboxes Hack
	$('input[type=checkbox]:not(:checked)').each(function(){
	    fdata = fdata+'&'+$(this).attr('id')+'=off';
	});
	// Define Action Method
	fdata = fdata+'&action=updateOptionsAjax';
	// Ajax Post
	$.post(ajaxurl, fdata, function(response){
	    if(response == 1){
		$('.notifications').append('<div class="myth-notification myth-success fade">Changes Successfully Saved.</div>');
	    } else {
		$('.notifications').append('<div class="myth-notification myth-error fade">Error Saving Changes. Please try again later.</div>');
	    }
	    initNoticeDismissal();
	    scrollToElem($('.notifications'));
	});
	return false;
    });
});

function add_dismiss_notification(){
    $('.myth-notification').append('<p class="myth-dismiss">Click this box to dismiss.</p>');
}

function dismiss_notification(element){
    element.fadeOut();
}

function showSubCat(cat){
    var elemattr = 'ul[for="'+cat+'"]';
    var elem = $(elemattr);
    if (elem.css('display') === 'none'){
        hideSubCats();
        $(elem).slideDown();
    }
}

function initIphoneStyle(){
   // Initialize iPhone Style Checkboxes
   $('.onoff').iphoneStyle();
   $('.yesno').iphoneStyle({
        checkedLabel : 'YES',
        uncheckedLabel : 'NO'
    }); 
}

function hideSubCats(){
    $('.myth-subcat').slideUp();
}

function hideSubCatPages(){
    $('.mythSCFormContainer').hide();
}

function showSubCatPage(elm){
    // hide the other pages first
    hideSubCatPages();
    div = 'div#' + elm.attr('id');
    $(div).slideDown(200);
    initIphoneStyle();
}

function createSliders(){
    $('.mythSliderOption').each(function(){
	// Get Options for Slider
	sliderelm = $('.theMythSlider', this);
	slidermin = $('.sliderMin', this).val();
	slidermax = $('.sliderMax', this).val();
	sliderstep = $('.sliderStep', this).val();
	sliderval = $('.sliderVal', this).val();

	// Create Slider
	sliderelm.slider({
	    animate: true,
	    value: sliderval,
	    min: slidermin,
	    max: slidermax,
	    step: sliderstep,
	    slide: function(event, ui){
		updatelm = $('.sliderVal', this);
		$(updatelm).val(ui.value);
	    }
	});
    });
}

function initNoticeDismissal(){
    $('.myth-notification').click(function(){
	$(this).fadeOut();
    });
}

function scrollToElem(elem){
    $('html, body').animate({
        scrollTop: 0
    }, 1000);
}

