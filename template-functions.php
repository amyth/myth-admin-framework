<?php
// Package Name: MythAdmin (v) 1.0.1
// Script Name: MythAdmin Template Functions
// Copyright (c) 2012 Techstricks.com, Amyth Arora. All rights reserved.

function isTrue($optname){
    $val = get_option($optname);
    if($val == 'on'){
        return true;
    } else {
        return false;
    }
}
?>