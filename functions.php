<?php
// Package Name: MythAdmin (v) 1.0.1
// Script Name: MythAdmin Main Functions
// Copyright (c) 2012 Techstricks.com, Amyth Arora. All rights reserved.
//
// Released under the GNU General Public License
// http://www.gnu.org/licenses/gpl-2.0.html
//
// This is a buddypress theme with highly configurable options for Wordpress
// http://wordpress.org/
//
// Thanks to God (For making me capable of doing stuff like this)
// Thanks to My Family for Supporting me (Literally)
// Thanks to Wordpress ( http://www.wordpress.org ).
// Thanks to BuddyPress ( http://www.buddypress.org ).
//
// **********************************************************************
// This is a premium wordpress/buddypress theme and is exclusively
// distributed by http://www.themeforest.net on behalf of the theme
// author (Amyth Arora). If you bought this theme and are facing problems
// setting it up, feel free to post to the support forums for this
// theme on http://www.themeforest.net
// **********************************************************************
//
//You should have received a copy of the GNU General Public License
//along with this program.  If not, see <http://www.gnu.org/licenses/>

// Importand Imports
require('mythadmin/mythadmin.php');

//Initiate MythAdmin Framework
$mad = new MythAdmin();
$mad->register_theme();

// Theme Setup Hook
add_action( 'after_setup_theme', 'mythadmin_theme_installer' );
function mythadmin_theme_installer(){

}
?>